package com.hk.coba.dao;

import com.hk.coba.model.PesertaModel;
import com.hk.coba.model.ProdiModel;
import com.hk.coba.model.UniversityModel;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by harunakaze on 30-Mar-17.
 */
@Mapper
public interface UniversityMapper {
    @Select("SELECT kode_univ, nama_univ, url_univ " +
            "FROM univ")
    @Results(value = {
            @Result(property = "kode_univ", column = "kode_univ"),
            @Result(property = "nama_univ", column = "nama_univ"),
            @Result(property = "url_univ", column = "url_univ"),
            @Result(property = "daftar_prodi", column = "kode_univ",
                    javaType = List.class,
                    many=@Many(select = "selectProdi"))
    })
    List<UniversityModel> selectAllUniversity();

    @Select("SELECT kode_univ, nama_univ, url_univ " +
            "FROM univ " +
            "WHERE kode_univ = #{kode_univ}")
    @Results(value = {
            @Result(property = "kode_univ", column = "kode_univ"),
            @Result(property = "nama_univ", column = "nama_univ"),
            @Result(property = "url_univ", column = "url_univ"),
            @Result(property = "daftar_prodi", column = "kode_univ",
                    javaType = List.class,
                    many=@Many(select = "selectProdi")),
            @Result(property = "daftar_peserta", column = "kode_univ",
                    javaType = List.class,
                    many=@Many(select = "selectPeserta"))
    })
    UniversityModel selectUniversity(@Param("kode_univ") String kode_univ);

    @Select("SELECT kode_univ, nama_univ, url_univ " +
            "FROM univ " +
            "WHERE kode_univ = #{kode_univ}")
    @Results(value = {
            @Result(property = "kode_univ", column = "kode_univ"),
            @Result(property = "nama_univ", column = "nama_univ"),
            @Result(property = "url_univ", column = "url_univ"),
            @Result(property = "daftar_prodi", column = "kode_univ",
                    javaType = List.class,
                    many=@Many(select = "selectProdi"))
    })
    UniversityModel selectUniversityWithoutPeserta(@Param("kode_univ") String kode_univ);

    @Select("SELECT kode_univ, nama_univ, url_univ " +
            "FROM univ " +
            "JOIN prodi USING (kode_univ) " +
            "WHERE kode_prodi = #{kode_prodi}")
    UniversityModel selectUniversityByKodeProdi(@Param("kode_prodi") String kode_prodi);

    @Delete("DELETE FROM univ " +
            "WHERE kode_univ = #{kode_univ}")
    int deleteUniversity(@Param("kode_univ") String kode_univ);

    @Insert("INSERT INTO univ " +
            "(kode_univ, nama_univ, url_univ) " +
            "VALUES" +
            "(#{kode_univ}, #{nama_univ}, #{url_univ})")
    int addUniversity(UniversityModel university);

    @Update("UPDATE univ SET " +
            "nama_univ = #{nama_univ}, " +
            "url_univ = #{url_univ} " +
            "WHERE kode_univ = #{kode_univ}")
    int updateUniversity(UniversityModel university);

    @Select("SELECT kode_prodi, nama_prodi " +
            "FROM prodi " +
            "WHERE kode_univ = #{kode_univ}")
    List<ProdiModel> selectProdi(@Param("kode_univ") String kode_univ);

    @Select("SELECT nomor, nama, tgl_lahir " +
            "FROM peserta " +
            "JOIN prodi USING (kode_prodi) " +
            "JOIN univ USING(kode_univ) " +
            "WHERE kode_univ = #{kode_univ}")
    @Results(value = {
            @Result(property = "tanggalLahir", column = "tgl_lahir")
    })
    List<PesertaModel> selectPeserta(@Param("kode_univ") String kode_univ);
}
