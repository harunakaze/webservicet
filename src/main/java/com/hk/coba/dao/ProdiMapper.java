package com.hk.coba.dao;

import com.hk.coba.model.PesertaModel;
import com.hk.coba.model.ProdiModel;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by harunakaze on 30-Mar-17.
 */
@Mapper
public interface ProdiMapper {

    @Select("SELECT kode_univ, kode_prodi, nama_prodi " +
            "FROM prodi " +
            "WHERE kode_prodi = #{kode_prodi}")
    @Results(value = {
            @Result(property = "kode_univ", column = "kode_univ"),
            @Result(property = "kode_prodi", column = "kode_prodi"),
            @Result(property = "nama_prodi", column = "nama_prodi"),
            @Result(property = "daftar_peserta", column = "kode_prodi",
                    javaType = List.class,
                    many=@Many(select = "selectPeserta"))
    })
    ProdiModel selectProdi(@Param("kode_prodi") String kode_prodi);

    @Select("SELECT kode_univ, kode_prodi, nama_prodi " +
            "FROM prodi")
    @Results(value = {
            @Result(property = "kode_univ", column = "kode_univ"),
            @Result(property = "kode_prodi", column = "kode_prodi"),
            @Result(property = "nama_prodi", column = "nama_prodi")
    })
    List<ProdiModel> selectAllProdi();

    @Select("SELECT kode_prodi, nama_prodi " +
            "FROM prodi " +
            "WHERE kode_univ = #{kode_univ}")
    List<ProdiModel> selectProdiFromUniv(@Param("kode_univ") String kode_univ);

    @Select("SELECT nomor, nama, tgl_lahir " +
            "FROM peserta " +
            "WHERE kode_prodi = #{kode_prodi}")
    @Results(value = {
            @Result(property = "tanggalLahir", column = "tgl_lahir")
    })
    List<PesertaModel> selectPeserta(@Param("kode_prodi") String kode_prodi);

    @Insert("INSERT INTO prodi " +
            "(kode_univ, kode_prodi, nama_prodi) " +
            "VALUES" +
            "(#{kode_univ}, #{kode_prodi}, #{nama_prodi})")
    int addProdi(ProdiModel university);

    @Update("UPDATE prodi SET " +
            "nama_prodi = #{nama_prodi} " +
            "WHERE kode_prodi = #{kode_prodi}")
    int updateProdi(ProdiModel university);

    @Delete("DELETE FROM prodi " +
            "WHERE kode_prodi = #{kode_prodi}")
    int deleteProdi(@Param("kode_prodi") String kode_prodi);
}
