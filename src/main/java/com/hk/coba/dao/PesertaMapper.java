package com.hk.coba.dao;

import com.hk.coba.model.PesertaModel;
import org.apache.ibatis.annotations.*;

/**
 * Created by harunakaze on 07-Apr-17.
 */
@Mapper
public interface PesertaMapper {

    @Select("SELECT nomor, nama, tgl_lahir, kode_prodi " +
            "FROM peserta " +
            "WHERE nomor = #{nomor}")
    @Results(value = {
            @Result(property = "nomor", column = "nomor"),
            @Result(property = "nama", column = "nama"),
            @Result(property = "tanggalLahir", column = "tgl_lahir"),
            @Result(property = "kode_prodi", column = "kode_prodi")
    })
    PesertaModel selectPeserta(@Param("nomor") String nomor);

    @Insert("INSERT INTO peserta " +
            "(nomor, nama, tgl_lahir, kode_prodi) " +
            "VALUES" +
            "(#{nomor}, #{nama}, #{tanggalLahir}, #{kode_prodi})")
    int addPeserta(PesertaModel peserta);

    @Update("UPDATE peserta SET " +
            "nama = #{nama}," +
            "tgl_lahir = #{tanggalLahir}," +
            "kode_prodi = #{kode_prodi} " +
            "WHERE nomor = #{nomor}")
    int updatePeserta(PesertaModel peserta);

    @Delete("DELETE FROM peserta " +
            "WHERE nomor = #{nomor}")
    int deletePeserta(String nomor);
}
