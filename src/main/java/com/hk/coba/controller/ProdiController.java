package com.hk.coba.controller;

import com.hk.coba.model.PesertaModel;
import com.hk.coba.model.ProdiModel;
import com.hk.coba.model.UniversityModel;
import com.hk.coba.service.PesertaService;
import com.hk.coba.service.ProdiService;
import com.hk.coba.service.UniversityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by harunakaze on 08-Apr-17.
 */
@Controller
public class ProdiController {

    @Autowired
    ProdiService prodiService;

    @Autowired
    UniversityService universityService;

    @Autowired
    PesertaService pesertaService;

    @RequestMapping("/prodi")
    public String selectProdi(Model model,
                              @RequestParam("kode") String kode_prodi) {
        ProdiModel prodi = prodiService.selectProdi(kode_prodi);

        if(prodi == null) {
            model.addAttribute("kode_prodi", kode_prodi);
            return "prodi-notfound";
        }

        UniversityModel university = universityService.selectUniversity(prodi.getKode_univ());
        assert university != null;

        model.addAttribute("university", university);
        model.addAttribute("prodi", prodi);

        List<PesertaModel> daftarPeserta = prodi.getDaftar_peserta();

        if(daftarPeserta.size() != 0) {
            PesertaModel oldest = pesertaService.getOldest(daftarPeserta);
            PesertaModel youngest =  pesertaService.getYoungest(daftarPeserta);

            model.addAttribute("oldest", oldest);
            model.addAttribute("youngest", youngest);
        }

        return "prodi-view";
    }

    @RequestMapping("/prodi/delete/{kode_prodi}")
    public String deleteProdi(Model model,
                                   @PathVariable(value = "kode_prodi") String kode_prodi) {
        ProdiModel prodi = prodiService.selectProdi(kode_prodi);

        if (prodi != null) {
            int status = prodiService.deleteProdi (kode_prodi);
            return "redirect:/univ/" + prodi.getKode_univ();
        } else {
            model.addAttribute("kode_prodi", kode_prodi);
            return "prodi-notfound";
        }
    }

    @RequestMapping("/prodi/add")
    public String addProdi(Model model,
                           @RequestParam("kode_univ") String kode_univ) {
        UniversityModel university = universityService.selectUniversityWithoutPeserta(kode_univ);
        if(university == null) {
            model.addAttribute("kode_univ", kode_univ);
            return "university-notfound";
        }

        ProdiModel prodi = new ProdiModel();
        prodi.setKode_univ(kode_univ);

        model.addAttribute("prodi", prodi);
        return "prodi/create";
    }

    @RequestMapping(value = "prodi/add/submit", method = RequestMethod.POST)
    public String addSubmitUniversity(@Valid @ModelAttribute("university") ProdiModel prodi,
                                      BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            return "prodi/create";
        }

        int status = prodiService.addProdi(prodi);
        if(status == 0) {
            return "error-page";
        }

        return "redirect:/univ/" + prodi.getKode_univ();
    }

    @RequestMapping("/prodi/update/{kode_prodi}")
    public String updateProdi(Model model, @PathVariable(value = "kode_prodi") String kode_prodi) {
        ProdiModel prodi = prodiService.selectProdi(kode_prodi);

        if (prodi == null) {
            model.addAttribute("kode_prodi", kode_prodi);
            return "prodi-notfound";
        }

        model.addAttribute("prodi", prodi);
        return "prodi/update";
    }

    @RequestMapping("/prodi/update/submit")
    public String updateProdiSubmit(@Valid @ModelAttribute(value = "prodi") ProdiModel prodi,
                                         BindingResult bindingResult){
        if(bindingResult.hasErrors()) {
            return "prodi/update";
        }

        int status = prodiService.updateProdi(prodi);
        if(status == 0) {
            return "error-page";
        }

        return "redirect:/prodi?kode=" + prodi.getKode_prodi();
    }
}
