package com.hk.coba.controller;

import com.hk.coba.model.PesertaModel;
import com.hk.coba.model.ProdiModel;
import com.hk.coba.service.PesertaService;
import com.hk.coba.service.ProdiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by harunakaze on 22-Apr-17.
 */
@RestController
public class PesertaRestController {

    @Autowired
    PesertaService pesertaService;

    @RequestMapping(value = "/rest/peserta", method = RequestMethod.GET)
    public PesertaModel selectPeserta (Model model,
                                 @RequestParam(value = "nomor", required = false) String nomorPeserta){
        PesertaModel peserta = pesertaService.selectPeserta(nomorPeserta);

        return peserta;
    }
}
