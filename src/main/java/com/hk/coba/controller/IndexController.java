package com.hk.coba.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by harunakaze on 30-Mar-17.
 */
@Controller
public class IndexController {

    @RequestMapping("/")
    public String index (Model model,
                         @RequestParam(value = "nomor", required = false) String nomor){
        if(nomor == null) {
            nomor = "";
        }
        model.addAttribute("nomorPeserta", nomor);
        return "home";
    }
}
