package com.hk.coba.controller;

import com.hk.coba.model.PesertaModel;
import com.hk.coba.model.UniversityModel;
import com.hk.coba.service.PesertaService;
import com.hk.coba.service.UniversityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by harunakaze on 30-Mar-17.
 */
@Controller
public class UniversityController {

    @Autowired
    UniversityService universityService;

    @Autowired
    PesertaService pesertaService;

    @RequestMapping("/univ")
    public String listUniversitas(Model model) {
        List<UniversityModel> universities = universityService.selectAllUniversity();
        model.addAttribute("universities", universities);

        return "universitas-all";
    }

    @RequestMapping("/univ/{kode_univ}")
    public String viewUniversity(Model model,
                                 @PathVariable(value = "kode_univ") String kode_univ) {
        UniversityModel university = universityService.selectUniversityWithoutPeserta(kode_univ);

        if(university == null) {
            model.addAttribute("kode_univ", kode_univ);
            return "university-notfound";
        }

        model.addAttribute("university", university);

        return "university";
    }

    @RequestMapping("/univ/peserta/{kode_univ}")
    public String viewPesertaUniversity(Model model,
                                 @PathVariable(value = "kode_univ") String kode_univ) {
        UniversityModel university = universityService.selectUniversity(kode_univ);

        if(university == null) {
            model.addAttribute("kode_univ", kode_univ);
            return "university-notfound";
        }

        List<PesertaModel> daftarPeserta = university.getDaftar_peserta();

        if(daftarPeserta.size() != 0) {
            PesertaModel oldest = pesertaService.getOldest(daftarPeserta);
            PesertaModel youngest =  pesertaService.getYoungest(daftarPeserta);

            model.addAttribute("oldest", oldest);
            model.addAttribute("youngest", youngest);
        }

        model.addAttribute("university", university);

        return "university-peserta";
    }

    @RequestMapping("/univ/delete/{kode_univ}")
    public String deleteUniversity(Model model,
                                   @PathVariable(value = "kode_univ") String kode_univ) {
        UniversityModel student = universityService.selectUniversityWithoutPeserta(kode_univ);

        if (student != null) {
            int status = universityService.deleteUniversity (kode_univ);
            return "delete-success";
        } else {
            model.addAttribute ("kode_univ", kode_univ);
            return "not-found";
        }
    }

    @RequestMapping("/univ/add")
    public String addUniversity(Model model) {
        model.addAttribute("university", new UniversityModel());
        return "university-add";
    }

    @RequestMapping(value = "univ/add/submit", method = RequestMethod.POST)
    public String addSubmitUniversity(@Valid @ModelAttribute("university") UniversityModel university,
                                      BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            return "university-add";
        }

        int status = universityService.addUniversity(university);
        if(status == 0) {
            return "error-page";
        }

        return "redirect:/univ/" + university.getKode_univ();
    }

    @RequestMapping("/univ/update/{kode_univ}")
    public String updateUniversity(Model model, @PathVariable(value = "kode_univ") String kode_univ) {
        UniversityModel um = universityService.selectUniversityWithoutPeserta(kode_univ);

        if (um == null) {
            return "error-page";
        }

        model.addAttribute("university", um);
        return "university-update";
    }

    @RequestMapping("/univ/update/submit")
    public String updateSubmitUniversity(@Valid @ModelAttribute(value = "university") UniversityModel university,
                                         BindingResult bindingResult){
        if(bindingResult.hasErrors()) {
            return "university-update";
        }

        int status = universityService.updateUniversity(university);
        if(status == 0) {
            return "error-page";
        }

        return "redirect:/univ/" + university.getKode_univ();
    }
}
