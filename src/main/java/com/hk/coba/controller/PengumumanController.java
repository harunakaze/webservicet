package com.hk.coba.controller;

import com.hk.coba.model.PesertaModel;
import com.hk.coba.model.ProdiModel;
import com.hk.coba.model.UniversityModel;
import com.hk.coba.service.PesertaService;
import com.hk.coba.service.ProdiService;
import com.hk.coba.service.UniversityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by harunakaze on 07-Apr-17.
 */
@Controller
public class PengumumanController {

    @Autowired
    PesertaService pesertaService;

    @Autowired
    UniversityService universityService;

    @Autowired
    ProdiService prodiService;

    @RequestMapping(value = "/pengumuman/submit", method = RequestMethod.POST)
    public String pengumumanSubmit (Model model,
                                    @RequestParam(value = "nomorpeserta") String nomorPeserta){
        PesertaModel peserta = pesertaService.selectPeserta(nomorPeserta);

        if(peserta == null) {
            model.addAttribute("nomorPeserta", nomorPeserta);
            return "pengumuman-notfound";
        }

        UniversityModel university = universityService.selectUniversityByKodeProdi(peserta.getKode_prodi());
        assert university != null;

        ProdiModel prodi = prodiService.selectProdi(peserta.getKode_prodi());
        assert prodi != null;

        model.addAttribute("university", university);
        model.addAttribute("prodi", prodi);
        model.addAttribute("peserta", peserta);
        return "pengumuman-found";
    }
}
