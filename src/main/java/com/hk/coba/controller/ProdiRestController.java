package com.hk.coba.controller;

import com.hk.coba.model.PesertaModel;
import com.hk.coba.model.ProdiModel;
import com.hk.coba.model.UniversityModel;
import com.hk.coba.service.ProdiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by harunakaze on 22-Apr-17.
 */
@RestController
public class ProdiRestController {

    @Autowired
    ProdiService prodiService;

    @RequestMapping(value = "/rest/prodi", method = RequestMethod.GET)
    public ProdiModel selectProdi(Model model,
                              @RequestParam(value = "kode", required = false) String kode_prodi) {
        ProdiModel prodi = prodiService.selectProdi(kode_prodi);

        return prodi;
    }

}
