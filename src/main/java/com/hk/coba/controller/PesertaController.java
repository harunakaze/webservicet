package com.hk.coba.controller;

import com.hk.coba.model.PesertaModel;
import com.hk.coba.model.ProdiModel;
import com.hk.coba.model.UniversityModel;
import com.hk.coba.service.PesertaService;
import com.hk.coba.service.ProdiService;
import com.hk.coba.service.UniversityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by harunakaze on 08-Apr-17.
 */
@Controller
public class PesertaController {

    @Autowired
    PesertaService pesertaService;

    @Autowired
    ProdiService prodiService;

    @Autowired
    UniversityService universityService;

    @RequestMapping("/peserta")
    public String selectPeserta (Model model,
                                    @RequestParam("nomor") String nomorPeserta){
        PesertaModel peserta = pesertaService.selectPeserta(nomorPeserta);

        if(peserta == null) {
            model.addAttribute("nomorPeserta", nomorPeserta);
            return "peserta-notfound";
        }
        ProdiModel prodi = prodiService.selectProdi(peserta.getKode_prodi());

        model.addAttribute("prodi", prodi);
        model.addAttribute("peserta", peserta);
        return "peserta-detail";
    }

    @RequestMapping("/peserta/delete/{nomor}")
    public String deletePeserta(Model model,
                              @PathVariable(value = "nomor") String nomorPeserta) {
        PesertaModel peserta = pesertaService.selectPeserta(nomorPeserta);

        if (peserta != null) {
            int status = pesertaService.deletePeserta (nomorPeserta);
            return "redirect:/prodi?kode=" + peserta.getKode_prodi();
        } else {
            model.addAttribute("peserta", peserta);
            return "peserta-notfound";
        }
    }

    @RequestMapping("/peserta/add")
    public String addPeserta(Model model,
                             @RequestParam(value = "kode_prodi", required = false) String kode_prodi) {
        if(kode_prodi != null) {
            ProdiModel prodi = prodiService.selectProdi(kode_prodi);

            if(prodi == null) {
                model.addAttribute("kode_prodi", kode_prodi);
                return "prodi-notfound";
            }
        } else {
            kode_prodi = "";
        }

        PesertaModel peserta = new PesertaModel();
        peserta.setKode_prodi(kode_prodi);

        List<UniversityModel> daftarUniversitas = universityService.selectAllUniversity();

        model.addAttribute("daftarUniversitas", daftarUniversitas);
        model.addAttribute("peserta", peserta);
        return "peserta/create";
    }

    @RequestMapping(value = "peserta/add/submit", method = RequestMethod.POST)
    public String addSubmitPeserta(Model model,
                                   @Valid @ModelAttribute("university") PesertaModel peserta,
                                   BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            return "peserta/create";
        }

        if(!peserta.getKode_prodi().trim().isEmpty()) {
            ProdiModel prodi = prodiService.selectProdi(peserta.getKode_prodi());

            if(prodi == null) {
                model.addAttribute("kode_prodi", peserta.getKode_prodi());
                return "prodi-notfound";
            }
        } else {
            peserta.setKode_prodi(null);
        }

        int status = pesertaService.addPeserta(peserta);
        if(status == 0) {
            return "error-page";
        }

        return "redirect:/peserta?nomor=" + peserta.getNomor();
    }

    @RequestMapping("/peserta/update/{nomorPeserta}")
    public String updatePeserta(Model model, @PathVariable(value = "nomorPeserta") String nomorPeserta) {
        PesertaModel peserta = pesertaService.selectPeserta(nomorPeserta);

        if(peserta == null) {
            model.addAttribute("nomorPeserta", nomorPeserta);
            return "peserta-notfound";
        }

        List<UniversityModel> daftarUniversitas = universityService.selectAllUniversity();

        model.addAttribute("daftarUniversitas", daftarUniversitas);
        model.addAttribute("peserta", peserta);
        return "peserta/update";
    }

    @RequestMapping("/peserta/update/submit")
    public String updateSubmitPeserta(@Valid @ModelAttribute(value = "prodi") PesertaModel peserta,
                                    BindingResult bindingResult){
        if(bindingResult.hasErrors()) {
            return "peserta/update";
        }

        int status = pesertaService.updatePeserta(peserta);
        if(status == 0) {
            return "error-page";
        }

        return "redirect:/peserta?nomor=" + peserta.getNomor();
    }
}
