package com.hk.coba.service;

import com.hk.coba.model.ProdiModel;

import java.util.List;

/**
 * Created by harunakaze on 30-Mar-17.
 */
public interface ProdiService {
    ProdiModel selectProdi(String kode_prodi);
    List<ProdiModel> selectAllProdi();

    int addProdi(ProdiModel university);

    int updateProdi(ProdiModel university);

    int deleteProdi(String kode_prodi);
    List<ProdiModel> selectProdiFromUniv(String kode_univ);
}
