package com.hk.coba.service;

import com.hk.coba.dao.PesertaMapper;
import com.hk.coba.model.PesertaModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by harunakaze on 31-Mar-17.
 */
@Service
public class PesertaServiceDatabase implements PesertaService {

    @Autowired
    private PesertaMapper pesertaMapper;

    @Override
    public PesertaModel selectPeserta(String nomor) {
        PesertaModel peserta = pesertaMapper.selectPeserta(nomor);
        if(peserta != null) {
            setUmur(peserta);
        }

        return peserta;
    }

    @Override
    public int addPeserta(PesertaModel peserta) {
        return pesertaMapper.addPeserta(peserta);
    }

    @Override
    public int updatePeserta(PesertaModel peserta) {
        return pesertaMapper.updatePeserta(peserta);
    }

    @Override
    public int deletePeserta(String nomor) {
        return pesertaMapper.deletePeserta(nomor);
    }

    @Override
    public PesertaModel getOldest(List<PesertaModel> listPeserta) {
        PesertaModel peserta = Collections.max(listPeserta, (first, second) -> second.getTanggalLahir().compareTo(first.getTanggalLahir()));
        setUmur(peserta);
        return peserta;
    }

    @Override
    public PesertaModel getYoungest(List<PesertaModel> listPeserta) {
        PesertaModel peserta = Collections.min(listPeserta, (first, second) -> second.getTanggalLahir().compareTo(first.getTanggalLahir()));
        setUmur(peserta);
        return peserta;
    }



    private void setUmur(PesertaModel peserta) {
        Date tanggalLahir = peserta.getTanggalLahir();
        Instant instant = Instant.ofEpochMilli(tanggalLahir.getTime());
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        LocalDate localDate = localDateTime.toLocalDate();

        long age = localDate.until(LocalDate.now(), ChronoUnit.YEARS);

        peserta.setUmur((int)age);
    }
}
