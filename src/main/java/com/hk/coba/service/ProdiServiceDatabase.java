package com.hk.coba.service;

import com.hk.coba.dao.ProdiMapper;
import com.hk.coba.model.ProdiModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by harunakaze on 07-Apr-17.
 */
@Service
public class ProdiServiceDatabase implements ProdiService {

    @Autowired
    private ProdiMapper prodiMapper;


    @Override
    public ProdiModel selectProdi(String kode_prodi) {
        return prodiMapper.selectProdi(kode_prodi);
    }

    @Override
    public List<ProdiModel> selectAllProdi() {
        return prodiMapper.selectAllProdi();
    }

    @Override
    public int addProdi(ProdiModel university) {
        return prodiMapper.addProdi(university);
    }

    @Override
    public int updateProdi(ProdiModel university) {
        return prodiMapper.updateProdi(university);
    }

    @Override
    public int deleteProdi(String kode_prodi) {
        return prodiMapper.deleteProdi(kode_prodi);
    }

    @Override
    public List<ProdiModel> selectProdiFromUniv(String kode_univ) {
        return prodiMapper.selectProdiFromUniv(kode_univ);
    }
}
