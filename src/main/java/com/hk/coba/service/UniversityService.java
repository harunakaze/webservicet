package com.hk.coba.service;

import com.hk.coba.model.UniversityModel;

import java.util.List;

/**
 * Created by harunakaze on 30-Mar-17.
 */
public interface UniversityService {
    UniversityModel selectUniversity(String kode_univ);
    UniversityModel selectUniversityWithoutPeserta(String kode_univ);
    UniversityModel selectUniversityByKodeProdi(String kode_prodi);

    List<UniversityModel> selectAllUniversity();

    int addUniversity(UniversityModel university);

    int updateUniversity(UniversityModel university);

    int deleteUniversity(String kode_univ);
}
