package com.hk.coba.service;

import com.hk.coba.model.PesertaModel;

import java.util.List;

/**
 * Created by harunakaze on 31-Mar-17.
 */
public interface PesertaService {
    PesertaModel selectPeserta(String nomor);

    int addPeserta(PesertaModel peserta);

    int updatePeserta(PesertaModel peserta);

    int deletePeserta(String nomor);

    PesertaModel getOldest(List<PesertaModel> listPeserta);
    PesertaModel getYoungest(List<PesertaModel> listPeserta);
}
