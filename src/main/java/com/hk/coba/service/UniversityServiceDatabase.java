package com.hk.coba.service;

import com.hk.coba.dao.UniversityMapper;
import com.hk.coba.model.PesertaModel;
import com.hk.coba.model.UniversityModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

/**
 * Created by harunakaze on 30-Mar-17.
 */
@Service
public class UniversityServiceDatabase implements UniversityService {

    @Autowired
    private UniversityMapper universityMapper;

    @Override
    public UniversityModel selectUniversity(String kode_univ) {
        UniversityModel results = universityMapper.selectUniversity(kode_univ);

        if(results != null) {
            setAge(results.getDaftar_peserta());
        }

        return results;
    }

    @Override
    public UniversityModel selectUniversityWithoutPeserta(String kode_univ) {
        UniversityModel results = universityMapper.selectUniversityWithoutPeserta(kode_univ);

        return results;
    }

    @Override
    public UniversityModel selectUniversityByKodeProdi(String kode_prodi) {
        return universityMapper.selectUniversityByKodeProdi(kode_prodi);
    }

    @Override
    public List<UniversityModel> selectAllUniversity() {
        return universityMapper.selectAllUniversity();
    }

    @Override
    public int addUniversity(UniversityModel university) {
        return universityMapper.addUniversity(university);
    }

    @Override
    public int updateUniversity(UniversityModel university) {
        return universityMapper.updateUniversity(university);
    }

    @Override
    public int deleteUniversity(String kode_univ) {
        return universityMapper.deleteUniversity(kode_univ);
    }

    private void setAge(List<PesertaModel> daftarPeserta) {
        for (PesertaModel peserta : daftarPeserta) {
            Date tanggalLahir = peserta.getTanggalLahir();
            Instant instant = Instant.ofEpochMilli(tanggalLahir.getTime());
            LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
            LocalDate localDate = localDateTime.toLocalDate();

            long age = localDate.until(LocalDate.now(), ChronoUnit.YEARS);

            peserta.setUmur((int) age);
        }
    }
}
