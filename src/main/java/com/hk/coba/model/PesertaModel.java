package com.hk.coba.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by harunakaze on 30-Mar-17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PesertaModel {
    String nomor;
    String nama;
    Date tanggalLahir;
    String kode_prodi;
    int umur;
}
