package com.hk.coba.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by harunakaze on 30-Mar-17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UniversityModel {
    @NotNull
    @NotBlank
    String kode_univ;
    @NotNull
    @NotBlank
    String nama_univ;
    String url_univ;
    List<ProdiModel> daftar_prodi;
    List<PesertaModel> daftar_peserta;
}
