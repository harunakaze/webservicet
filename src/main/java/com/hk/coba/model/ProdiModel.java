package com.hk.coba.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by harunakaze on 30-Mar-17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProdiModel {
    String kode_univ;
    String kode_prodi;
    String nama_prodi;
    List<PesertaModel> daftar_peserta;
}
